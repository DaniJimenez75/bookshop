import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  private API_BOOKS = "http://localhost:8181/api/books";

  constructor(private http: HttpClient) { }

  public getAllBooks(): Observable<any>{
      return this.http.get(this.API_BOOKS + "/getAll");
  }
}
