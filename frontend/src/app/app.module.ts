import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import {MatTableModule} from '@angular/material/table';
import { AppComponent } from './app.component';
import { BooksComponent } from './components/books/books.component';
import { HomeComponent } from './components/home/home.component';



import { HttpClientModule } from '@angular/common/http';
import { MenuComponent } from './components/menu/menu.component';
import { BookEditComponent } from './components/book-edit/book-edit.component';
import { FilterPipe } from './components/pipes/filter.pipe';


const appRoutes:Routes=[

  {path:'', component:HomeComponent},
  {path: 'books', component:BooksComponent},
  {path: 'editBook/:idBook', component:BookEditComponent}
  ];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BooksComponent,
    MenuComponent,
    BookEditComponent,
    FilterPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatTableModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes) 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
