import { Component, OnDestroy, OnInit } from '@angular/core';
import { BooksService } from 'src/app/services/books.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit{

books: any= {};
filterBooks = '';

  constructor(private service: BooksService, private route:ActivatedRoute,
    private router: Router){

  }

  ngOnInit(): void {

    this.service.getAllBooks().subscribe(books => {
      this.books = books;
      });
  }

  goBookEdit(idBook: number){
    this.router.navigate(["/editBook", idBook]);
  }




}
