package com.demo.controller;

import com.demo.entity.BookEntity;
import com.demo.service.BookService;
import com.demo.service.BookServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Component
@RestController
@RequestMapping("api/books")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/getAll")
    public List<BookEntity> listBooks(){
        return bookService.listBooks();
    }

    @PostMapping("/savebook")
    public BookEntity saveBook(@RequestBody BookEntity book) {
        return bookService.saveBook(book);
    }

    @GetMapping("/getbook/{id}")
    public BookEntity getBook(@PathVariable(name="id") Long id) {
        return bookService.getBook(id);
    }

    @PutMapping("/updatebook/{id}")
    public BookEntity actualizarEmpleado(@PathVariable(name="id")Long id,@RequestBody BookEntity book) {

        BookEntity bookSelected= bookService.getBook(id);

        bookSelected.setName(book.getName());
        bookSelected.setAuthor(book.getAuthor());
        bookSelected.setPrice(book.getPrice());
        bookSelected.setStock(book.getStock());
        bookSelected.setDescription(book.getDescription());

        return bookService.updateBook(bookSelected);
    }

    @DeleteMapping("/deletebook/{id}")
    public void deleteBook(@PathVariable(name="id")Long id) {
        bookService.deleteBook(id);
    }

}
