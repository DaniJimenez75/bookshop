package com.demo.service;

import com.demo.entity.BookEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;


public interface BookService {

    ArrayList<BookEntity> listBooks();

    BookEntity saveBook(BookEntity empleado);

    BookEntity getBook(Long id);

    BookEntity updateBook(BookEntity empleado);

    void deleteBook(Long id);

}
